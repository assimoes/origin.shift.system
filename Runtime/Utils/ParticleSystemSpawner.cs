﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSystemSpawner : MonoBehaviour
{
    public ParticleSystem source;

    [SerializeField]
    Vector3 startPosition;

    [SerializeField]
    int incrementStep = 100;

    [SerializeField]
    int spawnCount = 1000;
    private void Awake()
    {
        for (int i = 0; i < spawnCount; i++)
        {
            var position = new Vector3(startPosition.x, startPosition.y, startPosition.z + incrementStep);
            var ps = Instantiate(source, position, Quaternion.identity);
            var em = ps.emission;
            em.enabled = true;

            EntityTracker.Instance.TrackParticleSystem(ps);

            startPosition = position;
        }
    }

}
