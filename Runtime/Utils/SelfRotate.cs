﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfRotate : MonoBehaviour
{

    [SerializeField]
    private float rotationRatio = 0.1f;
    void Start()
    {
                
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.Rotate(0, rotationRatio, 0);
    }
}
