﻿using System.IO;
using UnityEngine;


namespace Utils
{
    public class PerformanceLogger : IPerformanceLogger
    {

        private float m_time;
        private float m_elapsed;

        private float m_global_elapsed;

        private readonly string filePath;
        private readonly bool debugMode;
        private readonly string session;

        public PerformanceLogger(string filePath, string session, bool debugMode = true)
        {
            this.filePath = filePath;
            this.debugMode = debugMode;
            this.session = session;
        }

        public PerformanceLogger()
        { }

        public void Begin()
        {
            m_time = Time.realtimeSinceStartup;
        }

        public void End()
        {
            m_elapsed = ((Time.realtimeSinceStartup - m_time) * 1000);
            m_global_elapsed += m_elapsed;
        }

        public void Write(string function,int frame)
        {
            if (debugMode)
                Debug.Log(string.Format("{0}\n{1}ms.",function, m_elapsed.ToString("f6")));
            
            using (StreamWriter writer = new StreamWriter(filePath, true))
            {
                writer.WriteLine(string.Format("{0},{1},{2},{3}",session,frame,function, m_elapsed.ToString("f6")));
            }
        }

        public void WriteGlobal(string function, int frame)
        {
            if (debugMode)
                Debug.Log(string.Format("{0}\n{1}ms.", function, m_global_elapsed.ToString("f6")));

            using (StreamWriter writer = new StreamWriter(filePath, true))
            {
                writer.WriteLine(string.Format("{0},{1},{2},{3}", session, frame, function, m_global_elapsed.ToString("f6")));
            }

            m_global_elapsed = 0f;
        }
    }

    public interface IPerformanceLogger
    {
        void Begin();
        void End();
        void Write(string function, int frame);
        void WriteGlobal(string function, int frame);
    }
}
