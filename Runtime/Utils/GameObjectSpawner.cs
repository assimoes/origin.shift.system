﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectSpawner : MonoBehaviour
{
    public GameObject source;

    [SerializeField]
    Vector3 startPosition;

    [SerializeField]
    int incrementStep = 100;

    [SerializeField]
    int spawnCount = 1000;
    private void Awake()
    {
        for (int i = 0; i < spawnCount; i++)
        {
            var position = new Vector3(startPosition.x, startPosition.y, startPosition.z + incrementStep);
            var go = Instantiate(source, position, Quaternion.identity);
            go.SetActive(true);

            EntityTracker.Instance.TrackGameObject(go);

            startPosition = position;
        }
    }

}
