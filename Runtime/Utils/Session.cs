﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Session 
{
    private static string m_current;
    public static string Current 
    {
        get
        {
            if (string.IsNullOrWhiteSpace(m_current))
            {
                m_current = DateTime.Now.ToString("yyyyMMddHHssffff");
                return m_current;
            }
            else
            {
                return m_current;
            }
        }
    }
}
