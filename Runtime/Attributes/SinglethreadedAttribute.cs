using System;


namespace OriginShift.Attributes
{   
    [AttributeUsage(AttributeTargets.Class, Inherited = true)]
    public class SinglethreadedAttribute : Attribute
    { }   
}