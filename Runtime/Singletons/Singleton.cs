﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.PackageManager;
using UnityEngine;


/* Obtido em
 * http://wiki.unity3d.com/index.php/Singleton
 */
public class Singleton<T> : MonoBehaviour
    where T : MonoBehaviour
{
    private static bool m_shuttingdown = false;
    private static object m_lock = new object();
    private static T m_instance;

    public static T Instance
    {
        get
        {
            if (m_shuttingdown) return null;

            lock(m_lock)
            {
                if (m_instance == null)
                {
                    m_instance = (T)FindObjectOfType(typeof(T));
                    if (m_instance == null)
                    {
                        var singleton = new GameObject();
                        m_instance = singleton.AddComponent<T>();
                        singleton.name = typeof(T).ToString() + "_singleton";

                        DontDestroyOnLoad(singleton);
                    }
                }

                return m_instance;
            }
        }
    }

    private void OnApplicationQuit()
    {
        m_shuttingdown = true;
    }

    private void OnDestroy()
    {
        m_shuttingdown = true;        
    }

}
