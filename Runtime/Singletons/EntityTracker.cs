﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

public class EntityTracker : Singleton<EntityTracker>
{
    private  List<GameObject> m_gameObjects = null;
    private  List<ParticleSystem> m_particleSystems = null;

    public event Action OnGameObjectAdded;
    public event Action OnParticleSystemAdded;
    public event Action OnGameObjectRemoved;
    public event Action OnParticleSystemRemoved;
    

    public GameObject[] GetRootGameObjects()
    {
        if (m_gameObjects == null)
            m_gameObjects = SceneManager.GetActiveScene().GetRootGameObjects().ToList<GameObject>();

        return m_gameObjects.ToArray<GameObject>();
    }

    public ParticleSystem[] GetParticleSystems()
    {
        if (m_particleSystems == null)
            m_particleSystems = FindObjectsOfType<ParticleSystem>().ToList<ParticleSystem>();

        return m_particleSystems.ToArray<ParticleSystem>();
    }

    public void TrackGameObject(GameObject gameObject)
    {
        if (m_gameObjects == null)
            m_gameObjects = SceneManager.GetActiveScene().GetRootGameObjects().ToList<GameObject>();
        else
            m_gameObjects.Add(gameObject);

        OnGameObjectAdded?.Invoke();
    }

    public void TrackParticleSystem(ParticleSystem particleSystem)
    {
        if (m_particleSystems == null)
            m_particleSystems = FindObjectsOfType<ParticleSystem>().ToList<ParticleSystem>();
        else
            m_particleSystems.Add(particleSystem);

        OnParticleSystemAdded?.Invoke();
    }

    public void UntrackGameObject(GameObject gameObject)
    {
        if (m_gameObjects == null)
            m_gameObjects = SceneManager.GetActiveScene().GetRootGameObjects().ToList<GameObject>();
        else
            m_gameObjects.Remove(gameObject);
    
        OnGameObjectRemoved?.Invoke();
    }

    public void UntrackParticleSystem (ParticleSystem particleSystem)
    {
        if (m_particleSystems == null)
            m_particleSystems = FindObjectsOfType<ParticleSystem>().ToList<ParticleSystem>();
        else
            m_particleSystems.Remove(particleSystem);
    
        OnParticleSystemRemoved?.Invoke();
    }
}
