﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(VirtualCoordinateOriginShiftTarget))]
public class OriginCameraController : MonoBehaviour
{
    public float speed = 1f;
    VirtualCoordinate virtualCoordinate;
    void Start()
    {
        virtualCoordinate = GetComponent<VirtualCoordinateOriginShiftTarget>().virtualCoordinate;
    }

    void Update()
    {
        var delta = this.transform.forward * speed * Time.deltaTime;
        this.transform.position += delta;

        if (virtualCoordinate != null)
        {
            virtualCoordinate.SetCoordinates(
                delta.x,
                delta.y,
                delta.z
            );
        }

    }
}
