﻿using UnityEngine;
using OriginShift.ShiftRunners;
using Utils;

namespace OriginShift.Services 
{
    /// <summary>
    /// Serviço cuja responsabilidade é receber uma lista de IShifters e executar o método Shift.
    /// </summary>
    public class OriginShiftRunnerService : OriginShifterRunner
    {
        public override void Run(Vector3 cameraPosition, params IShifter[] originShifters)
        {
            foreach (var shifter in originShifters)
            {
                shifter.Shift(cameraPosition);
            }
        }
    }
}
