using UnityEngine;
using System;
using Unity.Jobs;

namespace OriginShift.ShiftRunners
{
    public interface IShifter
    {
        /// <summary>
        /// Executa a transla��o dos objectos.
        /// Este m�todo � executado pelos objecto que implementa o interface IShiftRunner.
        /// </summary>
        /// <param name="cameraPosition">Posi��o da camera virtual antes de ser deslocada para a origem do sistema de coordenadas</param>
        void Shift(Vector3 cameraPosition);

        JobHandle BeginShift(Vector3 cameraPosition);
        void CompleteShift(JobHandle jobHandle);
    }
}