using UnityEngine;
using System;

namespace OriginShift.ShiftRunners
{
    public interface IShiftRunner
    {
        /// <summary>
        /// Executa, sequencialmente, o m�todo Shift dos plugins de transla��o.
        /// Dever� existir apenas um objecto, por cena, que implementa este interface.
        /// </summary>
        /// <param name="cameraPosition">posi��o da camera virtual antes de ser deslocada para a origem</param>
        /// <param name="originShifters">list de plugins que executam a transla��o dos objectos no mundo em rela��o � camera virtual.</param>
        void Run(Vector3 cameraPosition,params IShifter[] originShifters);
    }
}