﻿using System.Collections;
using System.Collections.Generic;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Jobs;

namespace OriginShift.Jobs 
{
    [BurstCompile]
    public struct RootObjectOriginShiftParallelJob : IJobParallelForTransform
    {
        [ReadOnly]
        public Vector3 cameraPosition;
        
        public void Execute(int index, TransformAccess transform)
        {
            transform.position -= cameraPosition;
        }
    }
}