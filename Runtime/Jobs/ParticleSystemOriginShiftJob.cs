﻿using System.Collections;
using System.Collections.Generic;
using Unity.Burst;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.ParticleSystemJobs;

namespace OriginShift.Jobs
{
    [BurstCompile]
    public struct ParticleSystemOriginShiftJob : IJobParticleSystem
    {
        public Vector3 cameraPosition;
        public void Execute(ParticleSystemJobData jobData)
        {
            var positions = jobData.positions;

            for (int i = 0; i < jobData.count; i++)
            {
                positions[i] -= cameraPosition;            
            }
        }
    }
}

