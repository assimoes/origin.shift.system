using UnityEngine;
using UnityEngine.ParticleSystemJobs;
using Unity.Jobs;
using OriginShift.Services;
using OriginShift.Jobs;
using OriginShift.Attributes;

namespace OriginShift.Shifters
{
    /// <summary>
    /// Executa a transla��o de todas as part�culas presentes na cena em rela��o � posi��o da camera virtual
    /// </summary>
    [Singlethreaded]
    public class ParticleSystemShifterSingle : Shifter
    {
        public override string Name { get => "ParticleSystemShifterSingle"; }

        private int m_currentFrame = 0;

        private ParticleSystem.Particle[] parts = null;


        ParticleSystem[] particleSystems = null;

        private bool debugMode;
        public override void Init(bool debug, string logfile)
        {
            Initialize(logfile, true);
            particleSystems = EntityTracker.Instance.GetParticleSystems();
            EntityTracker.Instance.OnParticleSystemAdded += RefreshParticleSystemList;
            EntityTracker.Instance.OnParticleSystemRemoved += RefreshParticleSystemList;
            this.debugMode = debug;
        }

        public override void Destroy()
        {
            particleSystems = null;

            EntityTracker.Instance.OnParticleSystemAdded -= RefreshParticleSystemList;
            EntityTracker.Instance.OnParticleSystemRemoved -= RefreshParticleSystemList;
        }
        private void RefreshParticleSystemList()
        {
            particleSystems = EntityTracker.Instance.GetParticleSystems();
        }

        public override void Shift(Vector3 cameraPosition)
        {

            _logger.Begin();

            /*
             * C�digo obtido/baseado em
             * http://wiki.unity3d.com/index.php/Floating_Origin
             */

            foreach (var ps in particleSystems)
            {
                if (ps.main.simulationSpace != ParticleSystemSimulationSpace.World)
                    continue;

                int maxParticles = ps.main.maxParticles;

                if (maxParticles <= 0)
                    continue;

                bool wasPaused = ps.isPaused;
                bool wasPlaying = ps.isPlaying;

                if (!wasPaused)
                    ps.Pause();

                if (parts == null || parts.Length < maxParticles)
                {
                    parts = new ParticleSystem.Particle[maxParticles];
                }

                int num = ps.GetParticles(parts);

                for (int i = 0; i < num; i++)
                {
                    parts[i].position -= cameraPosition;
                }

                ps.SetParticles(parts, num);

                if (wasPlaying)
                    ps.Play();
            }




            _logger.End();
            _logger.Write("ParticleSystemShifterSingle", m_currentFrame );

            m_currentFrame++;
        }

        public override JobHandle BeginShift(Vector3 cameraPosition)
        {
            throw new System.NotImplementedException();
        }

        public override void CompleteShift(JobHandle jobHandle)
        {
            throw new System.NotImplementedException();
        }

    }
}