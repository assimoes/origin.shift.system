using UnityEngine;
using OriginShift.Jobs;
using OriginShift.Services;
using Unity.Mathematics;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine.SceneManagement;
using OriginShift.Attributes;

using Utils;
using System;
using UnityEngine.Jobs;

namespace OriginShift.Shifters
{

    /// <summary>
    /// Desloca todos os GameObjects que n�o t�m pai. Ou seja, GameObjects que s�o a ra�z da hierarquia.
    /// Os objectos filho destes GameObjects s�o deslocados autom�ticamente em rela��o ao objecto ra�z a que pertencem.
    /// Esta opera��o � executada em paralelo em m�ltiplas threads.
    /// </summary>
    [Multithreaded]
    public class GameObjectShifterMulti : Shifter
    {


        public int batchSize = 10;

        public override string Name { get => "GameObjectShifterMulti"; }

        private int m_currentFrame = 0;

        private NativeArray<float3> rootObjectPositionsArray;
        private TransformAccessArray transformAccessArray;
        private GameObject[] rootObjects;
        private bool debugMode;

        public override void Destroy()
        {
            transformAccessArray.Dispose();

            EntityTracker.Instance.OnGameObjectAdded -= RefreshGameObjectList;
            EntityTracker.Instance.OnGameObjectRemoved -= RefreshGameObjectList;
        }
        public override void Init(bool debug, string logfile)
        {
            Initialize(logfile, true);

            // Obt�m lista de root gameobjects presentes na cena
            rootObjects = EntityTracker.Instance.GetRootGameObjects();
            // Copia as posi��es dos objectos para uma NativeArray<float3>.
            CopyGameObjectsPositionsToTransformAccessArray(rootObjects);

            EntityTracker.Instance.OnGameObjectAdded += RefreshGameObjectList;
            EntityTracker.Instance.OnGameObjectRemoved += RefreshGameObjectList;
            this.debugMode = debug;
        }

        private void RefreshGameObjectList()
        {
            rootObjects = EntityTracker.Instance.GetRootGameObjects();
        }
        /// <summary>
        /// Copia uma lista de GameObjects para dentro de uma NativeArray.
        /// </summary>
        /// <param name="source">Lista de GameObjects</param>
        /// <returns>NativeArray com as posi��es dos GameObjects</returns>
        NativeArray<float3> CopyGameObjectsPositionsToNativeArray(GameObject[] source)
        {
            NativeArray<float3> target = new NativeArray<float3>(source.Length, Allocator.TempJob);
            for (int i = 0; i < source.Length; i++)
            {
                // Ignora GameObjects que t�m a tag "noshift". Ex.: Managers e servi�os presentes na cena.
                if (source[i].tag.ToLower() != "noshift")
                {
                    target[i] = source[i].transform.position;
                }


            }
            return target;
        }

        void CopyGameObjectsPositionsToTransformAccessArray(GameObject[] source)
        {
            Transform[] target = new Transform[source.Length];

            for (int i = 0; i < source.Length; i++)
            {
                // Ignora GameObjects que t�m a tag "noshift". Ex.: Managers e servi�os presentes na cena.
                if (source[i].tag.ToLower() != "noshift")
                {
                    target[i] = source[i].transform;
                }

            }

            transformAccessArray = new TransformAccessArray(target);

        }

        /// <summary>
        /// Actualiza com as novas posi��es, presentes na NativeArray, a lista de GameObjects.
        /// Este m�todo � executado ap�s o Job estar concluido.
        /// </summary>
        /// <param name="positions">NativeArray com as posi��es actualizadas</param>
        /// <param name="target">Lista de GameObjects cujas posi��o dever�o ser actualizadas</param>
        void ApplyPositionsNativeArray(NativeArray<float3> positions, GameObject[] target)
        {
            for (int i = 0; i < target.Length; i++)
            {
                // Ignora gameobjects com a tag "noshift".
                if (target[i].tag.ToLower() != "noshift")
                {
                    target[i].transform.position = positions[i];
                }
            }
        }

        /// <summary>
        /// Executa a transla��o de todos os GameObject ra�z presentes na cena (incluindo a camera virtual).
        /// </summary>
        /// <param name="cameraPosition">posi��o da camera virtual antes de ser deslocada para a origem do sistema de coordenadas.</param>
        public override void Shift(Vector3 cameraPosition)
        {

            _logger.Begin();

            // Obt�m lista de root gameobjects presentes na cena
            rootObjects = SceneManager.GetActiveScene().GetRootGameObjects();

            // Copia as posi��es dos objectos para uma NativeArray<float3>.
            CopyGameObjectsPositionsToTransformAccessArray(rootObjects);

            // Cria o Job
            RootObjectOriginShiftParallelJob job = new RootObjectOriginShiftParallelJob
            {
                cameraPosition = cameraPosition
            };

            // Agenda o Job
            JobHandle jobHandle = job.Schedule(transformAccessArray);

            // For�a o bloqueio da main thread at� o job estar concluido
            jobHandle.Complete();

            // Aplica as novas posi��es � lista de gameobjects
            ApplyPositionsNativeArray(rootObjectPositionsArray, rootObjects);

            // Limpa a NativeArray da mem�ria
            rootObjectPositionsArray.Dispose();

            _logger.End();

            _logger.Write("GameObjectShifterMulti", m_currentFrame);

            m_currentFrame++;
        }

        public override JobHandle BeginShift(Vector3 cameraPosition)
        {
            _logger.Begin();

            // Cria o Job
            RootObjectOriginShiftParallelJob job = new RootObjectOriginShiftParallelJob
            {
                cameraPosition = cameraPosition
            };

            // Agenda o Job
            JobHandle jobHandle = job.Schedule(transformAccessArray);


            _logger.End();

            //_logger.Write("gameobjects_begin_shift", 0);

            return jobHandle;
        }

        public override void CompleteShift(JobHandle jobHandle)
        {
            _logger.Begin();

            jobHandle.Complete();

            _logger.End();

            //_logger.Write("gameobjects_end_shift", 0);
            _logger.WriteGlobal("gameobjects_global", 0);
        }
    }
}