using System;
using UnityEngine;
using OriginShift.ShiftRunners;
using Utils;
using Unity.Jobs;

public abstract class Shifter : IShifter
{
    /// <summary>
    /// Implementa��o do m�todo Shift contratualizado com o interface IShifter
    /// </summary>
    /// <param name="cameraPosition">posi��o da camera virtual antes de ser deslocada para a origem do sistema de coordenadas</param>
    public abstract void Shift(Vector3 cameraPosition);
    public abstract JobHandle BeginShift(Vector3 cameraPosition);
    public abstract void CompleteShift(JobHandle jobHandle);

    public abstract void Init(bool debug, string logFile);
    public abstract void Destroy();
    /// <summary>
    /// Descri��o ou nome do plugin
    /// </summary>
    public abstract string Name {get;}

    public IPerformanceLogger _logger;

    private string filePath;
    private bool debugMode;
     
    public void Initialize(string filePath,bool debugMode)
    {

        if (string.IsNullOrWhiteSpace(filePath)) throw new Exception("Must define a log file");

        this.filePath = filePath;
        this.debugMode = debugMode;
        _logger = new PerformanceLogger(filePath, Session.Current, debugMode);
    }
} 