using UnityEngine;
using UnityEngine.ParticleSystemJobs;
using Unity.Jobs;
using OriginShift.Services;
using OriginShift.Jobs;

namespace OriginShift.Shifters
{
    public class LogShifter : Shifter
    {
        public override string Name { get => "LogShifter"; }

        public override JobHandle BeginShift(Vector3 cameraPosition)
        {
            throw new System.NotImplementedException();
        }

        public override void CompleteShift(JobHandle jobHandle)
        {
            throw new System.NotImplementedException();
        }

        public override void Destroy()
        {
            throw new System.NotImplementedException();
        }

        public override void Init(bool debug, string logfile)
        {
            throw new System.NotImplementedException();
        }

        public override void Shift(Vector3 cameraPosition)
        {
            Debug.Log("This is a sample shifter.\n Does nothing other than logging to the debug console.");
        }
    }
}