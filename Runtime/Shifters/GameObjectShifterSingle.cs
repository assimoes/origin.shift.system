using UnityEngine;
using OriginShift.Jobs;
using OriginShift.Services;
using Unity.Mathematics;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine.SceneManagement;
using OriginShift.Attributes;

using Utils;
namespace OriginShift.Shifters
{
    /// <summary>
    /// Desloca todos os GameObjects que n�o t�m pai. Ou seja, GameObjects que s�o a ra�z da hierarquia.
    /// Os objectos filho destes GameObjects s�o deslocados autom�ticamente em rela��o ao objecto ra�z a que pertencem.
    /// Esta opera��o � executada na Main thread.
    /// </summary>
    [Singlethreaded]
    public class GameObjectShifterSingle : Shifter
    {

        public override string Name { get => "GameObjectShifterSingle"; }

        private int m_currentFrame = 0;

        private GameObject[] rootObjects;

        private bool debugMode;
        public override void Init(bool debug, string logfile)
        {

            Initialize(logfile, true);
            rootObjects = EntityTracker.Instance.GetRootGameObjects();

            EntityTracker.Instance.OnGameObjectAdded += RefreshGameObjectList;
            EntityTracker.Instance.OnGameObjectRemoved += RefreshGameObjectList;

            this.debugMode = debug;
        }

        public override void Destroy() 
        {
            EntityTracker.Instance.OnGameObjectAdded -= RefreshGameObjectList;
            EntityTracker.Instance.OnGameObjectRemoved -= RefreshGameObjectList;
        }
        private void RefreshGameObjectList()
        {
            rootObjects = EntityTracker.Instance.GetRootGameObjects();
        }
        public override void Shift(Vector3 cameraPosition)
        {
            _logger.Begin();

            /*
             * C�digo obtido/baseado em
             * http://wiki.unity3d.com/index.php/Floating_Origin
             */
            // Obt�m todos os objectos root presentes na cena.
            //GameObject[] rootObjects = SceneManager.GetActiveScene().GetRootGameObjects();
            
            foreach (GameObject obj in rootObjects)
            {
                // desloca o objecto em rela��o � posi��o da camera virtual.
                obj.transform.position -= cameraPosition;
            }

            _logger.End();

            _logger.Write("GameObjectShifterSingle", m_currentFrame);

            m_currentFrame++;
        }

        public override JobHandle BeginShift(Vector3 cameraPosition)
        {
            throw new System.NotImplementedException();
        }

        public override void CompleteShift(JobHandle jobHandle)
        {
            throw new System.NotImplementedException();
        }
    }
}