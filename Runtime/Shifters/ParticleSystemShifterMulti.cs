using UnityEngine;
using UnityEngine.ParticleSystemJobs;
using Unity.Jobs;
using OriginShift.Services;
using OriginShift.Jobs;
using OriginShift.Attributes;
using Unity.Collections;

namespace OriginShift.Shifters
{
    /// <summary>
    /// Executa a transla��o de todas as part�culas presentes na cena em rela��o � camera virtual
    /// Esta opera��o � executada em multiplas threads. 
    /// </summary>
    [Multithreaded]
    public class ParticleSystemShifterMulti : Shifter
    {
        
        public override string Name { get => "ParticleSystemShifterMulti"; }

        private int m_currentFrame = 0;

        private NativeArray<JobHandle> handles;

        private ParticleSystem[] particleSystems;

        private bool debugMode;
        public override void Init(bool debug, string logfile)
        {
            Initialize(logfile, true);
            // Obt�m todos os sistemas de particulas presentes na cena.
            particleSystems = EntityTracker.Instance.GetParticleSystems();
            handles = new NativeArray<JobHandle>(particleSystems.Length, Allocator.TempJob);

            EntityTracker.Instance.OnParticleSystemAdded += RefreshParticleSystemList;
            EntityTracker.Instance.OnParticleSystemRemoved += RefreshParticleSystemList;
            this.debugMode = debug;
        }

        private void RefreshParticleSystemList()
        {
            particleSystems = EntityTracker.Instance.GetParticleSystems();
        }

        public override void Destroy()
        {
            handles.Dispose();

            EntityTracker.Instance.OnParticleSystemAdded -= RefreshParticleSystemList;
            EntityTracker.Instance.OnParticleSystemRemoved -= RefreshParticleSystemList;
        }

        public override void Shift(Vector3 cameraPosition)
        {
            _logger.Begin();
            // Obt�m todos os sistemas de particulas presentes na cena.
            ParticleSystem[] particleSystems = Object.FindObjectsOfType<ParticleSystem>();
            
            // Se existir pelo menos um sistema de part�culas
            if (particleSystems.Length > 0)
            {
                /*
                 * Por cada sistema de part�culas, apenas s�o considerados aqueles cuja
                 * simula��o ocorra no espa�o global. Os sistemas de particulas simulados localmente,
                 * s�o deslocados correctamente quando o objecto pai � deslocado.
                 * 
                 * O ciclo segue para a pr�xima itera��o se a simula��o n�o for no espa�o global.
                 */
                foreach (ParticleSystem particleSystem in particleSystems)
                {
                    if (particleSystem.main.simulationSpace != ParticleSystemSimulationSpace.World )
                        continue;
                    
                    // Inst�ncia o Job de part�culas
                    ParticleSystemOriginShiftJob job = new ParticleSystemOriginShiftJob
                    {
                        // passa a posi��o da camera virtual para dentro do Job.
                        cameraPosition = cameraPosition
                    };

                    // Agenda o Job e assigna o resultado a uma JobHandle
                    var particleJobHandle = job.Schedule(particleSystem);

                    // Espera pelo final do Job antes de prosseguir.
                    // A main thread � bloqueada at� o job estar concluido.
                    particleJobHandle.Complete(); 
                }
            }

            _logger.End();
            _logger.Write("ParticleSystemShifterMulti", m_currentFrame);

            m_currentFrame++;
        }

        public override JobHandle BeginShift(Vector3 cameraPosition)
        {
            _logger.Begin();

            // Se existir pelo menos um sistema de part�culas
            if (particleSystems.Length > 0)
            {
                /*
                 * Por cada sistema de part�culas, apenas s�o considerados aqueles cuja
                 * simula��o ocorra no espa�o global. Os sistemas de particulas simulados localmente,
                 * s�o deslocados correctamente quando o objecto pai � deslocado.
                 * 
                 * O ciclo segue para a pr�xima itera��o se a simula��o n�o for no espa�o global.
                 */
                for (int i = 0; i < particleSystems.Length; i++)
                {
                    if (particleSystems[i].main.simulationSpace != ParticleSystemSimulationSpace.World)
                        continue;

                    // Inst�ncia o Job de part�culas
                    ParticleSystemOriginShiftJob job = new ParticleSystemOriginShiftJob
                    {
                        // passa a posi��o da camera virtual para dentro do Job.
                        cameraPosition = cameraPosition
                    };

                    // Agenda o Job e assigna o resultado a uma JobHandle
                    handles[i] = job.Schedule(particleSystems[i]);
                    
                    // Espera pelo final do Job antes de prosseguir.
                    // A main thread � bloqueada at� o job estar concluido.
                }
            }

            _logger.End();
            //_logger.Write("multithreadedParticles_begin", 0);
            return JobHandle.CombineDependencies(handles);

        }

        public override void CompleteShift(JobHandle jobHandle)
        {
            _logger.Begin();
            
            jobHandle.Complete();

            _logger.End();
            //_logger.Write("multithreadedParticles_end", 0);
            _logger.WriteGlobal("multithreadedParticles_global", 0);
        }
    }
}