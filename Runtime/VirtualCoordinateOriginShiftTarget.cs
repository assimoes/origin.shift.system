﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Componente de Coordenadas Virtuais
/// Deve ser colocado como componente do objecto alvo das translações para a origem do sistema de coordenadas.
/// Por norma, deverá ser um componente da Camera Virtual que segue o jogador.
/// </summary>
[DisallowMultipleComponent]
public class VirtualCoordinateOriginShiftTarget : MonoBehaviour
{
    public VirtualCoordinate virtualCoordinate;

}
