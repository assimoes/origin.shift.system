﻿using UnityEngine;
using OriginShift.Services;
using OriginShift.ShiftRunners;
using System.Reflection;
using System.Linq;
using System;
using System.Collections.Generic;
using OriginShift.Attributes;
using Unity.Jobs;

namespace OriginShift.Managers
{
    public class OriginShiftManager : MonoBehaviour
    {
        // O objecto de referencia para a deslocação para a origem do sistema de coordenadas
        // Por norma deverá ser sempre a camera virtual que segue o jogador.
        public Transform originShiftTarget;

        // A partir de que distância a deslocação para a origem do sistema de coordenadas deve ocorrer.
        public float threshold = 1000f;

        // Executa Shifters em múltiplas threads?
        public bool RunOnMultipleThreads = true;

        public bool debugMode = true;
        public string logFile = "originshift_function_log.txt";

        // Verifica se no Editor de Unity a checkbox RunOnMultipleThreads foi clickada.
        // Quando é true, reinjecta a lista de IShifters apropriados.
        bool m_threadsChanged;

        // Lista de IShifters
        Shifter[] m_originShifters;

        // Referência do Origin Shift Runner
        OriginShifterRunner m_runner;

        private List<JobHandle> handles;

        private bool jobsScheduled = false;


        /// <summary>
        /// Retorna coordenadas virtuais do alvo da deslocação para a origem dos sistema de coordenadas.
        /// Deverá ser sempre a camera virtual que segue o jogador.
        /// </summary>
        /// <returns>Coordenadas Virtuais</returns>
        public VirtualCoordinate GetVirtualCoordinateFromTarget()
        {
            return originShiftTarget
                .gameObject
                .GetComponent<VirtualCoordinateOriginShiftTarget>()
                .virtualCoordinate;
        }

        /// <summary>
        /// Equivalente ao constructor de uma classe.
        /// MonoBehaviours não permitem constructores com parâmetros.
        /// </summary>
        void Awake()
        {
            m_runner = GetComponent<OriginShiftRunnerService>();
            m_originShifters = ResolveShifters();
            m_threadsChanged = RunOnMultipleThreads;
        }

        private void Start()
        {
            foreach (var s in m_originShifters)
            {
                s.Init(debugMode, logFile);
            }
        }

        private void OnDisable()
        {
            foreach (var s in m_originShifters)
            {
                s.Destroy();
            }
        }
        /// <summary>
        /// Método executado em cada frame
        /// </summary>
        void Update()
        {
            // Verifica se, no editor, a opção mudou.
            if (m_threadsChanged != RunOnMultipleThreads)
            {
                // volta a resolver os IShifters
                m_originShifters = ResolveShifters();
                
                m_threadsChanged = RunOnMultipleThreads;
            }

            // Se a camera, ou outro objecto alvo, estiver a uma distância superior ao threshold definido em relação
            // à origem do sistema de coordenadas, executa a translação dos objectos.
            if (originShiftTarget.position.magnitude >= threshold)
            {
                Vector3 cameraPositionBeforeShift = originShiftTarget.position;

                handles = new List<JobHandle>();
                foreach (var s in m_originShifters)
                {
                    if (s.GetType().GetCustomAttribute<MultithreadedAttribute>() != null)
                    {
                        var h = s.BeginShift(cameraPositionBeforeShift);
                        handles.Add(h);
                    } else
                    {
                        s.Shift(cameraPositionBeforeShift);
                    }

                }
                

                jobsScheduled = true;
            }

        }

        /// <summary>
        /// Método executado em cada frame, sempre depois do métod Update.
        /// </summary>
        void LateUpdate()
        {
            if (jobsScheduled)
            {
                for (int i = 0; i < m_originShifters.Length; i++)
                {
                    if (m_originShifters[i].GetType().GetCustomAttribute<MultithreadedAttribute>() != null)
                    {
                        m_originShifters[i].CompleteShift(handles[i]);
                    }
                }

                handles.Clear();
                jobsScheduled = false;
            }
        }

        /// <summary>
        /// Recorrendo a reflexão, ou Reflection, instância as classes concretas que herdam e implementam
        /// a classe abstracta Shifter.
        /// </summary>
        /// <returns>Lista de classes concretas que herdam e implementam Shifter</returns>
        Shifter[] ResolveShifters()
        {
            var shifterTypes = Assembly.GetAssembly(typeof(Shifter)).GetTypes()
            .Where(t => t.IsClass && !t.IsAbstract && t.IsSubclassOf(typeof(Shifter)))
            .ToArray();

            List<Shifter> shifters = new List<Shifter>();



            for (int i = 0; i < shifterTypes.Length; i++)
            {
                // Se a opção para correr em multiplas thread estiver activa,
                // apenas instância Shifter que tenham o atributo Multithreaded.
                if (this.RunOnMultipleThreads)
                {
                    if (shifterTypes[i].GetCustomAttribute(typeof(MultithreadedAttribute), true) != null)
                    {
                        shifters.Add(Activator.CreateInstance(shifterTypes[i]) as Shifter);                        
                    }
                }

                // Se a opção para correr em multiplas thread estiver inactiva,
                // apenas instância Shifter que tenham o atributo Singlethreaded.
                if (!this.RunOnMultipleThreads)
                {
                    if (shifterTypes[i].GetCustomAttribute(typeof(SinglethreadedAttribute), true) != null)
                    {
                        shifters.Add(Activator.CreateInstance(shifterTypes[i]) as Shifter);                        
                    }
                }
            }

            return shifters.ToArray();
        }

    }   
}
