﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "AbsoluteCoordinates", menuName = "ScriptableObjects/OriginShift/AbsoluteCoordinates", order = 1)]
public class VirtualCoordinate : ScriptableObject
{
    Coordinate _coordinates;
    private Coordinate Coordinates 
    {
        get 
        {
            return _coordinates;
        }
        set
        {
            _coordinates = value;
        } 
    }

    /// <summary>
    /// Retorna as coordenadas virtuais
    /// </summary>
    /// <returns></returns>
    public Coordinate GetCoordinates()
    {
        return Coordinates;
    }

    /// <summary>
    /// Incrementa os deltas x, y e z às coordenadas virtuais
    /// </summary>
    /// <param name="x">delta da translação no eixo do x</param>
    /// <param name="y">delta da translação no eixo do y</param>
    /// <param name="z">delta da translação no eixo do z</param>
    public void SetCoordinates (float x, float y, float z)
    {
        _coordinates.X += x;
        _coordinates.Y += y;
        _coordinates.Z += z;
    }

    /// <summary>
    /// Entidade que armazena as posições virtuais
    /// É uma struct para que seja armazenada contíguamente em memória (stack)
    /// </summary>
    public struct Coordinate 
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }
    }
}
