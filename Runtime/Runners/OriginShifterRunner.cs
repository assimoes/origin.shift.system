using UnityEngine;
using System;

namespace OriginShift.ShiftRunners
{
    /// <summary>
    /// Classe abstracta que herda de MonoBehaviour para que a classe concreta possa ser adicionada como Componente a um GameObject de Unity
    /// Implementa o interface IShiftRunner
    /// </summary>
    public abstract class OriginShifterRunner : MonoBehaviour, IShiftRunner
    {
        /// <summary>
        /// Executa o m�todo Shift de cada IShiftRunner
        /// </summary>
        /// <param name="cameraPosition">Posi��o da camera virtual antes de ser deslocada para a origem do sistema de coordenadas</param>
        /// <param name="originShifters">Lista de IShifters</param>
        public abstract void Run(Vector3 cameraPosition, params IShifter[] originShifters);
    }
}