﻿using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EditorCustomWindows : EditorWindow
{

    UnityEngine.Vector2 scrollPos;
    UnityEngine.Vector2 scrollPosPS;

    bool showGameObjects = true;
    bool showParticleSystems = true;

    [MenuItem("Origin Shift/Show Virtual Coordinates Window")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(EditorCustomWindows)).title = "Origin Shift Window";
    }

    void OnGUI()
    {
        VirtualCoordinateOriginShiftTarget _virtualCoordinatesHolder = 
            FindObjectOfType<VirtualCoordinateOriginShiftTarget>();
        VirtualCoordinate v = _virtualCoordinatesHolder.virtualCoordinate;

        GUILayout.Label("Virtual Coordinates", EditorStyles.boldLabel);
        GUILayout.BeginHorizontal();
        GUILayout.Label("X");
        GUILayout.Label(v.GetCoordinates().X.ToString());
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        GUILayout.Label("Y");
        GUILayout.Label(v.GetCoordinates().Y.ToString());
        GUILayout.EndHorizontal();
        GUILayout.BeginHorizontal();
        GUILayout.Label("Z");
        GUILayout.Label(v.GetCoordinates().Z.ToString());
        GUILayout.EndHorizontal();

        showGameObjects = EditorGUILayout.Foldout(showGameObjects,"Show GameObjects");
        if (showGameObjects)
        {

            EditorGUILayout.BeginVertical();
            var objs = SceneManager.GetActiveScene().GetRootGameObjects().ToList().Take(50).ToArray();

            scrollPos = EditorGUILayout.BeginScrollView(scrollPos, GUILayout.Width(this.position.width), GUILayout.Height(this.position.height));
            for (int i = 0; i < objs.Length; i++)
            {
                objs[i] = (GameObject)EditorGUILayout.ObjectField((objs[i].transform.position - _virtualCoordinatesHolder.transform.position).ToString(), objs[i], typeof(GameObject));
            }
            EditorGUILayout.EndScrollView();
            EditorGUILayout.EndVertical();

        }
    }

    void Update()
    {
        Repaint();
    }
}
