﻿using OriginShift.Managers;
using OriginShift.Services;
using System;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

public static class EditorMenus
{

    [MenuItem("Origin Shift/Managers/Create Origin Shift Manager")]
    public static void CreateOriginShiftManager()
    {
        if (Object.FindObjectOfType<OriginCameraController>() == null)
        {
            Debug.LogError("You need an Origin Shift Target with an OriginCameraController component in the scene. Go to Origin Shift/Create Origin Shift Target");
            return;
        }
        
        if (Object.FindObjectOfType<OriginShiftManager>() != null) return;
        if (Object.FindObjectOfType<OriginShiftRunnerService>() != null) return;

        GameObject go = new GameObject("OriginShiftManager");
        go.AddComponent<OriginShiftRunnerService>();
        var manager = go.AddComponent<OriginShiftManager>();
        var target = Object.FindObjectOfType<OriginCameraController>();

        manager.originShiftTarget = target.transform;

    }

    [MenuItem("Origin Shift/Create Origin Shift Target")]
    public static void CreateOriginShiftTarget()
    {
        if (Object.FindObjectOfType<VirtualCoordinateOriginShiftTarget>() != null) return;
        if (Object.FindObjectOfType<OriginCameraController>() != null) return;
        GameObject manager = new GameObject("OriginShiftTarget");
        GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cube.AddComponent<SelfRotate>();
        Material mat = (Material)AssetDatabase.LoadAssetAtPath("Packages/com.pg20338.origin-shift-system/Runtime/Materials/Checker.mat", typeof(Material));

        cube.GetComponent<Renderer>().sharedMaterial = mat;
           
        cube.transform.parent = manager.transform;
        cube.transform.position = new Vector3(0, 0.4f, -7f);

        var virtualCoordinatesComponent = manager.AddComponent<VirtualCoordinateOriginShiftTarget>();
        var virtualCoordinates = ScriptableObject.CreateInstance<VirtualCoordinate>();
        virtualCoordinatesComponent.virtualCoordinate = virtualCoordinates;

        manager.AddComponent<OriginCameraController>();

        Camera camera = Object.FindObjectsOfType<Camera>().Where(c => c.tag == "MainCamera").FirstOrDefault();

        if (camera == null)
        {
            Debug.LogError("There has to be one Main Camera in the Scene");
            throw new Exception("there has to be one main camera in the scene");
        }

        camera.transform.parent = manager.transform;
    }

    [MenuItem("Origin Shift/Demos/Create GameObject Spawner")]
    public static void CreateGameObjectSpawner()
    {
        if (Object.FindObjectOfType<GameObjectSpawner>() != null) return;
        GameObject spawner = new GameObject("GameObjectSpawner");
        var s = spawner.AddComponent<GameObjectSpawner>();
        var gameObj = GameObject.CreatePrimitive(PrimitiveType.Cube);
        gameObj.transform.parent = spawner.transform;
        s.source = gameObj;
        gameObj.SetActive(false);
    }

    [MenuItem("Origin Shift/Demos/Create Particle System Spawner")]
    public static void CreateParticleSystemSpawner()
    {
        if (Object.FindObjectOfType<ParticleSystemSpawner>() != null) return;
        GameObject spawner = new GameObject("ParticleSystemSpawner");
        var o = new GameObject("particle system");
        var s = o.AddComponent<ParticleSystem>();
        s.simulationSpace = ParticleSystemSimulationSpace.World;
        var spawnerComponent = spawner.AddComponent<ParticleSystemSpawner>();
        spawnerComponent.source = s;
        s.transform.parent = spawner.transform;
        var em = s.emission;
        em.enabled = false;
    }
    
}
